Weibo::Application.routes.draw do
  resources :tags


  match 'users/upload_photo' => 'users#upload_photo'
  match 'users/submit_upload' => 'users#submit_upload', :via => 'post'
  match 'users/login' => 'users#login'
  match 'users/submit_login' => 'users#submit_login', :via => 'post'
  match 'users/logout' => 'users#logout'
  match 'users/tweets/:id' => 'users#tweets'
  match 'users/followers/:id' => 'users#followers'
  match 'users/follower/:id' => 'users#followers'
  match 'users/following/:id' => 'users#following'
  match 'users/followings/:id' => 'users#following'
  match 'users/add_following/:id' => 'users#add_following'
  match 'users/del_follower/:id' => 'users#del_follower'
  match 'users/del_following/:id' => 'users#del_following'
  match 'users/all' => 'users#all'
  match 'users/search' => 'users#search'
 
  resources :tags
  resources :tweets
  resources :users

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'users#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
