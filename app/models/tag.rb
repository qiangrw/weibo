class Tag < Neo4j::Rails::Model
  property :name, :type => String, :index => :exact

  has_n(:use_by_user).from(:user_tags)

end
