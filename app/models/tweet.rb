class Tweet < Neo4j::Rails::Model
  property :tweet_id, :type => String, :index => :exact
  property :text, :type => String, :index => :exact
  property :image, :type => String
  property :date, :type => DateTime, :index => :exact
  property :user_id, :type=>String
  
  def to_s
    tweet_id
  end
end
