class User < Neo4j::Rails::Model
  property :id, :type => String, :index => :exact
  property :name, :type => String, :index => :fulltext
  property :email, :type => String, :index => :exact
  property :password, :type => String
  property :location, :type => String
  property :gender, :type => String
  property :blog, :type => String
  property :intro, :type => String
  property :qq, :type => String
  property :msn, :type => String
  property :img, :type => String
  has_n :user_tags
  has_n :followers
  has_n :following
  has_n :tweets
  
  validates_presence_of :name
  validates_presence_of :email
  validates_uniqueness_of  :email

  #attr_accssor :password_confirmation
  #validates_confirmation_of:password
  #validate :password_non_blank
  
  def password_non_blank
     errors.add(:password,"Missing password") if password.blank?
  end

  def self.authenticate(email, password)
    user = self.find_by_email(email)
    if user
       if user.password != password
	  user = nil
       end
    end
    user
  end 
  
  def to_s
    name
  end
end
