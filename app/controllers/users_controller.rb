class UsersController < ApplicationController
  # POST /users/search
  def search
    @query = params[:q]
    @users = User.all("name : #{@query}", :type => :fulltext)
    #@tags = Tag.all("name :#{@query}",:type => :fulltext)
    #@tag_users = Array.new
    #@tags.each do |tag|
    #     tag.use_by_user.each do |tag_user|
    #     @tag_users << tag_user
    #	 end
    # end
    @tag_users = Array.new
    @tags = Tag.find_by_name(@query)
    if @tags != nil
       @tag_users = @tags.use_by_user
    end
    if session[:user_id] != nil
       @current_user = User.find(session[:user_id])
       @is_following = Array.new
       @tag_is_following = Array.new
       @users.each do |user|
           if @current_user.following.include?(user) || @current_user.email == user.email
                @is_following[user.id.to_i] = true
           else
		@is_following[user.id.to_i] = false
           end
       end
       @tag_users.each do |tag_user|
	   if @current_user.following.include?(tag_user) || @current_user.email == tag_user.email
                @tag_is_following[tag_user.id.to_i] = true
	   else
	        @tag_is_following[tag_user.id.to_i] = false
           end
       end

    else
       @is_following = nil
    end
    
    respond_to do |format|
      format.html # search.html.erb
      format.json { render :json => @users }
    end
  end

  # GET /users/all
  def all
    @users = User.all
    @isadmin = true

    respond_to do |format|
      format.html # all.html.erb
      format.json { render :json => @users }
    end
  end


  # GET /users
  # GET /users.json
  def index
    if session[:user_id] != nil
        @current_user = User.find(session[:user_id])
        @followings   = @current_user.following
        @tweets  = Array.new
        @current_user.tweets.each do |tweet|
            @tweets.push  tweet
        end
        @followings.each do |following|  
            following.tweets.each do |tweet|
               @tweets.push  tweet
            end
        end
        @tweets = @tweets.sort{|a,b|b.date.to_i <=> a.date.to_i}
    else
        respond_to do |format|
          format.html { redirect_to "/users/login" }
        end
        return
    end

    @user_name = Array.new
    @user_img = Array.new
    @user_id   = Array.new

    @tweets.each do |tweet|
        @user_id[tweet.id.to_i] = tweet.user_id
        find_user =  User.find(tweet.user_id) 
        @user_name[tweet.id.to_i] =  find_user.name
        @user_img[tweet.id.to_i] = find_user.img.nil? ? "default.png" : find_user.img
    end

    @mainpage = true
    respond_to do |format|
      format.html # index.html.erb
      format.json { render :json => @tweets }
    end
  end

  # GET /users/login
  def login
    respond_to do |format|
      format.html # login.html.erb
    end
  end

  # POST /users/submit_login
  def submit_login
      email = params[:email] 
      password = params[:password]
      user = User.authenticate(email,password) 
      @error = '用户名或密码错误'
      if user
	    session[:user_id] = user.id 
	    session[:user_name] = user.name 
	    redirect_to :action => 'index'
      else
        render 'users/login'
      end
  end

  # GET /users/logout
  def logout
    #TODO destroy session
    session[:user_id] = nil
    render 'users/login'  
  end


  # GET /users/tweets
  # GET /users/tweets.json
  def tweets
    @uid = params[:id]
    @user = User.find(@uid)
    @user.img = "default.png" if @user.img.nil?
    @isadmin = (session[:user_id] == @uid)
    @tweets = User.find(@uid).tweets
    @tweets = @tweets.sort{|a,b|b.date.to_i <=> a.date.to_i}
    @subhomepage = true
    respond_to do |format|
      format.html # tweets.html.erb
      format.json { render :json => @tweets }
    end
  end

  
  # GET /users/1
  # GET /users/1.json
  def show
    @uid = params[:id]
    #TODO checkdd session uid == userid
    @isadmin = (session[:user_id] == @uid)

    @user = User.find(params[:id]) #test
    @user.img = "default.png" if @user.img.nil?
    #TODO find user's tag
    @tags = @user.user_tags
    @subprofile = true
    respond_to do |format|
      format.html # show.html.erb
      format.json { render :json => @user }
    end
  end

  # GET /users/followers
  # GET /users/followers.json
  def followers
    @uid = params[:id]
    @user = User.find(@uid)
    @users = @user.followers
    @is_following = Array.new
    @current_user = User.find(session[:user_id])

    @users.each do |user|
      if @current_user.following.include?(user) || @current_user.email == user.email
         @is_following[user.id.to_i] = true
      else      
         @is_following[user.id.to_i] = false  
      end
    end

    @isadmin = true

    @subfollower = true
    respond_to do |format|
      format.html # followers .html.erb
      format.json { render :json => @users }
    end
  end

  # GET /users/followering
  # GET /users/followering.json
  def following
    @uid = params[:id]
    @user = User.find(params[:id])
    @users = @user.following
    @isadmin = true
    
    @is_following = Array.new
    @current_user = User.find(session[:user_id])
    
    @users.each do |user|
      if @current_user.following.include?(user) || @current_user.email == user.email
         @is_following[user.id.to_i] = true
      else
         @is_following[user.id.to_i] = false
      end
    end
    @subfollowing = true
    respond_to do |format|
      format.html # followering.html.erb
      format.json { render :json => @users}
    end
  end

  # GET /users/add_following
  def add_following
    @uid = params[:id]
    @follower = User.find(@uid)
    @user = User.find(session[:user_id])
    @users = @user.following

    if !@user.following.include?(@user)
    	@user.following << @follower
    	@follower.followers << @user
    	@user.save!
    	@follower.save!
    end

    respond_to do |format|
      format.html { redirect_to "/users/following/#{session[:user_id]}" }
      format.json { render :json => @users}
    end
  end

  # GET /users/del_follower
  def del_follower
    @uid = params[:id]
    @follower = User.find(@uid)
    @current_user = User.find(session[:user_id])
    @current_user.followers.delete(@follower)
    @follower.following.delete(@current_user)
    @current_user.save!
    @follower.save!
    @users = @current_user.followers
    respond_to do |format|
      format.html {redirect_to "/users/followers/#{session[:user_id]}"}
      format.json {render :json => @users}
    end
  end

  # GET /users/del_following
  def del_following
     @uid = params[:id]
     @following = User.find(@uid)
     @current_user = User.find(session[:user_id])
     @current_user.following.delete(@following)
     @following.followers.delete(@current_user)
     @current_user.save!
     @following.save!
     @users = @current_user.following
     respond_to do |format|
        format.html {redirect_to "/users/following/#{session[:user_id]}"}
        format.json {render :json => @users}
     end
  end


  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        session[:user_id] = @user.id
        session[:user_name] = @user.name
        format.html { redirect_to @user, :notice => 'User was successfully created.' }
        format.json { render :json => @user, :status => :created, :location => @user }
      else
        format.html { render :action => "new" }
        format.json { render :json => @user.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, :notice => 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :action => "edit" }
        format.json { render :json => @user.errors, :status => :unprocessable_entity }
      end
    end
  end


  # GET /users/upload_photo
  def upload_photo
    respond_to do |format|
      format.html # new.html.erb
      format.json { render :json => @user }
    end
  end

  # POST /users/submit_upload
  def submit_upload
      directory = "public/photo"
      name = params[:upload][:file].original_filename 
      type = ""
      if(name =~ /(jpe?g|png)/i) then
          type = $1
      else
          @error = "仅支持JPG和PNG格式"
      end
      name = session[:user_id] + "." + type
      @user = User.find(session[:user_id])
      @user.img = name
      @user.save!

      path = File.join(directory, name)
      File.open(path, "wb") { |f| f.write(params[:upload][:file].read) }

      respond_to do |format|
          if @error.nil?
              format.html { redirect_to @user, :notice => '用户头像更新成功' }
              format.json { head :no_content }
          else
              format.html { render :action => "edit" }
              format.json { render :json => @error }
          end
      end
  end
end
