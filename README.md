WeiboFinder with Neo4j graph database
==========

Installation
----------
    rvm install jruby-1.6.8
    rvm use jruby-1.6.8
    gem install rails
    git clone git@bitbucket.org:qiangrw/weibo.git
    cd weibo
    bundle install
    rails s

WorkFlow
----------
* git pull  # pull the latest code
* git diff  # see your change
* git add [file] # add your added/changed file
* git commit -a -m 'Comment about change'
* git push origin master # When a whole new function has been implemented
