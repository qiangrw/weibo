require 'spec_helper'

describe "users/edit" do
  before(:each) do
    @user = assign(:user, stub_model(User,
      :id => "MyString",
      :name => "MyString",
      :email => "MyString",
      :password => "MyString",
      :location => "MyString",
      :gender => "MyString",
      :blog => "MyString",
      :intro => "MyString",
      :qq => "MyString",
      :msn => "MyString"
    ))
  end

  it "renders the edit user form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form", :action => users_path(@user), :method => "post" do
      assert_select "input#user_id", :name => "user[id]"
      assert_select "input#user_name", :name => "user[name]"
      assert_select "input#user_email", :name => "user[email]"
      assert_select "input#user_password", :name => "user[password]"
      assert_select "input#user_location", :name => "user[location]"
      assert_select "input#user_gender", :name => "user[gender]"
      assert_select "input#user_blog", :name => "user[blog]"
      assert_select "input#user_intro", :name => "user[intro]"
      assert_select "input#user_qq", :name => "user[qq]"
      assert_select "input#user_msn", :name => "user[msn]"
    end
  end
end
