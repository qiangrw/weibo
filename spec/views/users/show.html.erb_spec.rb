require 'spec_helper'

describe "users/show" do
  before(:each) do
    @user = assign(:user, stub_model(User,
      :id => "Id",
      :name => "Name",
      :email => "Email",
      :password => "Password",
      :location => "Location",
      :gender => "Gender",
      :blog => "Blog",
      :intro => "Intro",
      :qq => "Qq",
      :msn => "Msn"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Id/)
    rendered.should match(/Name/)
    rendered.should match(/Email/)
    rendered.should match(/Password/)
    rendered.should match(/Location/)
    rendered.should match(/Gender/)
    rendered.should match(/Blog/)
    rendered.should match(/Intro/)
    rendered.should match(/Qq/)
    rendered.should match(/Msn/)
  end
end
