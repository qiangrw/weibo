require 'spec_helper'

describe "users/index" do
  before(:each) do
    assign(:users, [
      stub_model(User,
        :id => "Id",
        :name => "Name",
        :email => "Email",
        :password => "Password",
        :location => "Location",
        :gender => "Gender",
        :blog => "Blog",
        :intro => "Intro",
        :qq => "Qq",
        :msn => "Msn"
      ),
      stub_model(User,
        :id => "Id",
        :name => "Name",
        :email => "Email",
        :password => "Password",
        :location => "Location",
        :gender => "Gender",
        :blog => "Blog",
        :intro => "Intro",
        :qq => "Qq",
        :msn => "Msn"
      )
    ])
  end

  it "renders a list of users" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Id".to_s, :count => 2
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Password".to_s, :count => 2
    assert_select "tr>td", :text => "Location".to_s, :count => 2
    assert_select "tr>td", :text => "Gender".to_s, :count => 2
    assert_select "tr>td", :text => "Blog".to_s, :count => 2
    assert_select "tr>td", :text => "Intro".to_s, :count => 2
    assert_select "tr>td", :text => "Qq".to_s, :count => 2
    assert_select "tr>td", :text => "Msn".to_s, :count => 2
  end
end
