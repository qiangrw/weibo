require "rubygems"
require "neo4j-core"
require "rexml/document"

input = File.new "/home/yejianjun/rail/weibo/script/corpus/user_profile.xml"
doc = REXML::Document.new input
doc.elements.each("PERSONS/PERSON") do |person|
    node = User.new
    node[:id] =  person.elements[1].text
    node[:name] =  person.elements[1].text
    node[:email] =  "#{person.elements[1].text}@pku.edu.cn"
    node[:password] = "123"
    node[:gender] =  person.elements[2].text
    node[:location] =  person.elements[3].text
    node[:intro] =  person.elements[5].text
    node[:blog] =  person.elements[8].text
    node.save!
end
